import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";

export const mainStyles = makeStyles((theme: Theme) => ({
  root: {
    // display: "grid",
    // gridTemplateRows: "auto 1fr",
    // gridTemplateColumns: "240px 1fr",
    // gridTemplateAreas: `"header header" "sideBar main"`,
    minHeight: "200vh",
  },
  header: {},
  sideBar: {
    // gridArea: "sideBar",
    // borderRight: `1px solid ${theme.palette.divider}`,
  },
  main: {
    // gridArea: "main",
    // backgroundColor: theme.palette.background.default,
    // padding: theme.spacing(2, 3),
  },
}));

export const commonStyles = makeStyles((theme: Theme) => ({
  sizeBig:{
    height: theme.spacing(4)
  }
}));
