import MailIcon from "@mui/icons-material/Mail";

export const HEADER_TITLE = "Subject Rating Admin Page";

interface SideBarItem {
  icon: any;
  text: string;
  childens?: any;
}
export const SIDE_BAR_ITEMS: SideBarItem[] = [
  {
    icon: MailIcon,
    text: "User",
  },
  {
    icon: MailIcon,
    text: "Teacher",
  },
  {
    icon: MailIcon,
    text: "Student",
  },
  {
    icon: MailIcon,
    text: "Subject",
  },
];
