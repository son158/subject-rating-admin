import { Box } from "@mui/material";
import React, { useEffect } from "react";
import { mainStyles } from "../Style";
import Header from "./Header";
import SideBar from "./SideBar";

const Main = () => {
  const classes = mainStyles();

  document.addEventListener("keydown", function (event) {
    if (event.key === "Tab") {
      event.preventDefault();
      console.log("Tab");
    }
  });

  return (
    <Box className={classes.root}>
      <Box className={classes.header}>
        <Header />
      </Box>
      <Box className={classes.sideBar}>
        <SideBar />
      </Box>
      <Box className={classes.main}>
        Main
        {/* <Routes>
          <Route path="*" element={<NotFound />} />
          {navLinkItems.map(
            (item, index) =>
              item.component && <Route key={index} path={`/${item.linkTo}/*`} element={<item.component />} />,
          )}
        </Routes> */}
      </Box>
    </Box>
  );
};

export default Main;
