import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { RootState } from "../app/store";
import { SIDE_BAR_ITEMS } from "../constants";
import { layoutActions } from "./layoutSlice";

const SideBar = () => {
  const dispath = useAppDispatch();
  const isShow = useAppSelector((state: RootState) => state.layout.sideBarShow);
  const setShow = (show: boolean) => dispath(layoutActions.setSideBarShow(show));

  return (
    <>
      <SwipeableDrawer open={isShow} onClose={() => setShow(false)} onOpen={() => setShow(true)}>
        <Box
          sx={{ width: 400, marginTop: 20 }}
          role="presentation"
          onClick={() => setShow(true)}
          onKeyDown={() => setShow(false)}
        >
          <Divider />
          <List>
            {SIDE_BAR_ITEMS.map((item, index) => (
              <ListItem key={index} disablePadding>
                <ListItemButton>
                  <ListItemText primary={item.text} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
          <Divider />
        </Box>
      </SwipeableDrawer>
    </>
  );
};

export default SideBar;
