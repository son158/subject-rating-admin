import { Box } from "@mui/material";
import React, { useEffect } from "react";
import { mainStyles } from "../Style";
import Header from "./Header";
import SideBar from "./SideBar";

const Main = () => {
  const classes = mainStyles();
  const onTabDown = (event: KeyboardEvent) => {
    if (event.key === "Tab") {
      event.preventDefault();
      console.log("Tab");
    }
  };

  useEffect(() => {}, []);

  document.addEventListener("keydown", function (event) {
    console.log(`Key: ${event.key} with keycode ${event.keyCode} has been pressed`);
  });

  return (
    <Box className={classes.root}>
      <Box className={classes.header}>
        <Header />
      </Box>
      <Box className={classes.sideBar}>
        <SideBar />
      </Box>
      <Box className={classes.main}>
        Main
        {/* <Routes>
          <Route path="*" element={<NotFound />} />
          {navLinkItems.map(
            (item, index) =>
              item.component && <Route key={index} path={`/${item.linkTo}/*`} element={<item.component />} />,
          )}
        </Routes> */}
      </Box>
    </Box>
  );
};

export default Main;
