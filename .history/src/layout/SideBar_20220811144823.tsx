import React, { useState } from "react";
import Box from "@mui/material/Box";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

const SideBar = () => {
  const [isOpen, setOpen] = useState(false);

  const toggleDrawer = () => {
    setOpen(!isOpen);
  };

  const temp = ["Inbox", "Starred", "Send email", "Drafts"];
  return (
    <>
      <Button onClick={toggleDrawer}>Click Me</Button>
      <SwipeableDrawer open={isOpen} onClose={() => setOpen(false)} onOpen={() => setOpen(true)}>
        <Box sx={{ width: 250 }} role="presentation" onClick={() => setOpen(true)} onKeyDown={() => setOpen(false)}>
          <List>
            {temp.map((text, index) => (
              <ListItem key={index} disablePadding>
                <ListItemButton>
                  <ListItemText primary={text} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
          <Divider />
        </Box>
      </SwipeableDrawer>
    </>
  );
};

export default SideBar;
