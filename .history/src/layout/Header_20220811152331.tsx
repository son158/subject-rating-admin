import { Button, useScrollTrigger } from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Slide from "@mui/material/Slide";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import React from "react";
import { useAppDispatch } from "../app/hooks";
import { HEADER_TITLE } from "../constants";
import { layoutActions } from "./layoutSlice";

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
  children?: React.ReactElement;
}
function HideOnScroll(props: Props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children || <></>}
    </Slide>
  );
}

const Header = (props: Props) => {
  const dispath = useAppDispatch();

  return (
    <>
      <HideOnScroll {...props}>
        <AppBar color="primary">
          <Toolbar>
            <Typography variant="h5" component="div">
              <Button onClick={() => dispath(layoutActions.toggleSideBarShow())}>Click Me</Button>
              {HEADER_TITLE}
            </Typography>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <Toolbar />
    </>
  );
};

export default Header;
