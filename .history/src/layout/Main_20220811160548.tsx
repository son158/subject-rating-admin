import { Box } from "@mui/material";
import { mainStyles } from "../Style";
import Header from "./Header";
import SideBar from "./SideBar";

const Main = () => {
  const classes = mainStyles();
  // const dispath = useAppDispatch();
  // const isShow = useAppSelector((state: RootState) => state.layout.sideBarShow);

  // document.addEventListener("keydown", function (event) {
  //   if (event.key === "Tab") {
  //     event.preventDefault();
  //     dispath(layoutActions.setSideBarShow(!isShow));
  //   }
  // });

  return (
    <Box className={classes.root}>
      <Box className={classes.header}>
        <Header />
      </Box>
      <Box className={classes.sideBar}>
        <SideBar />
      </Box>
      <Box className={classes.main}>
        Main
        {/* <Routes>
          <Route path="*" element={<NotFound />} />
          {navLinkItems.map(
            (item, index) =>
              item.component && <Route key={index} path={`/${item.linkTo}/*`} element={<item.component />} />,
          )}
        </Routes> */}
      </Box>
    </Box>
  );
};

export default Main;
