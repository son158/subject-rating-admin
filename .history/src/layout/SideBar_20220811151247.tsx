import React, { useState } from "react";
import Box from "@mui/material/Box";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { RootState } from "../app/store";
import { layoutActions } from "./layoutSlice";
import { SIDE_BAR_ITEMS } from "../constants";

const SideBar = () => {
  const dispath = useAppDispatch();
  const isShow = useAppSelector((state: RootState) => state.layout.sideBarShow);
  const setShow = (show: boolean) => dispath(layoutActions.setSideBarShow(show));

  return (
    <>
      <SwipeableDrawer open={isShow} onClose={() => setShow(false)} onOpen={() => setShow(true)}>
        <Box sx={{ width: 250 }} role="presentation" onClick={() => setShow(true)} onKeyDown={() => setShow(false)}>
          <List>
            {SIDE_BAR_ITEMS.map((item, index) => (
              <ListItem key={index} disablePadding>
                <ListItemButton>
                  <ListItemText primary={item} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
          <Divider />
        </Box>
      </SwipeableDrawer>
    </>
  );
};

export default SideBar;
