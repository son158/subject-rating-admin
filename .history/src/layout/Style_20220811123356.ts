import { makeStyles } from "@mui/material";

export const mainStyles = makeStyles((theme) => ({
  root: {
    display: "grid",
    gridTemplateRows: "auto 1fr",
    gridTemplateColumns: "240px 1fr",
    gridTemplateAreas: `"header header" "sideBar main"`,

    minHeight: "100vh",
  },

  header: {
    gridArea: "header",
  },
  sideBar: {
    gridArea: "sideBar",
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  main: {
    gridArea: "main",
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(2, 3),
  },
}));
