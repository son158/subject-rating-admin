import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface LayoutState {
  sideBarShow: boolean;
}

const initialState: LayoutState = {
  sideBarShow: false,
};

const layoutSlice = createSlice({
  name: "Layout",
  initialState,
  reducers: {
    toggleSideBarShow: (state) => {
      state.sideBarShow = !state.sideBarShow;
    },
  },
});

export default layoutSlice;
