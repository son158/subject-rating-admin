import React from "react";
import MailIcon from "@mui/icons-material/Mail";

export const HEADER_TITLE = "Subject Rating Admin Page";

// export const SIDE_BAR_ITEMS = ["Inbox", "Starred", "Send email", "Drafts"];
interface SideBarItem {
  icon: any;
  text: string;
  childens?: any;
}
export const SIDE_BAR_ITEMS: SideBarItem[] = [
  {
    icon: MailIcon,
    text: "Inbox",
  },
];
