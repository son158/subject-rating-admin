import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface LayoutState {
  sideBarShow: boolean;
}

const initialState: LayoutState = {
  sideBarShow: false,
};

const layoutSlice = createSlice({
  name: "Layout",
  initialState,
  reducers: {
    toggleSideBarShow: (state) => {
      state.sideBarShow = !state.sideBarShow;
    },
    setSideBarShow: (state, action: PayloadAction<boolean>) => {
      state.sideBarShow = action.payload;
    },
  },
});

export const layoutActions = layoutSlice.actions;
const layoutReducer = layoutSlice.reducer;
export default layoutReducer;
