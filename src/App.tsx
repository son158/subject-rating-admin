import { CssBaseline } from "@mui/material";
import Main from "./layout/Main";

function App() {
  return (
    <div className="App">
      <Main />
      <CssBaseline />
    </div>
  );
}

export default App;
